module.exports = {

  port:process.env.PORT || 5432,
  db :{
    database: process.env.DB_NAME || 'shoppingdb',
    user:process.env.DB_USER || 'root',
    password:process.env.DB_PASS || '',
    options:{
      dialect:process.env.DIALECT || 'postgres',
      host:process.env.HOS||'localhost',


    }

  }

}
