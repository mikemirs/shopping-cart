const { Client } = require('pg')
const config = require('./config/config')


const client = new Client({
  user: config.db.user,
  host: config.db.options.host,
  database: config.db.database,
  password: config.db.password,
  port: config.port
});

async function connect() {
await client.connect();
return Promise.resolve('client connected');
}

const getProfile = (cb)=>{
  client.query('select * from profile', (err, res) => {
      if (err) return cb(err)
      cb(null,res.rows);
    })
}


const getProructs = (cb)=>{
  client.query('select * from products', (err, res) => {
      if (err) return cb(err)
      cb(null,res.rows);
    })
}
module.exports = {connect,getProfile,getProructs}
