/* eslint-disable */

import axios from 'axios';

const profile = {
  firstName: 'Joe',
  lastName: 'Montana',
  limit: 850.0
};

const products = [
  { id: 1, title: 'iPad 4 Mini', price: 500.01, inventory: 2, shipping: 15.0 },
  {
    id: 2,
    title: 'H&M T-Shirt White',
    price: 10.99,
    inventory: 10,
    shipping: 5.0
  },
  { id: 3, title: 'Nirvana - LP', price: 19.99, inventory: 3, shipping: 22.5 },
  {
    id: 4,
    title: 'Licensed Steel Gloves',
    price: 30.99,
    inventory: 5,
    shipping: 9.0
  },
  {
    id: 5,
    title: 'Rustic Granite Car',
    price: 487.0,
    inventory: 1,
    shipping: 35.0
  },
  {
    id: 6,
    title: 'Fantastic Cotton Pants',
    price: 59.59,
    inventory: 6,
    shipping: 11.0
  },
  {
    id: 7,
    title: 'Tasty Wooden Pizza',
    price: 29.0,
    inventory: 2,
    shipping: 18.0
  },
  {
    id: 8,
    title: 'Delicious Concrete Fish',
    price: 12.99,
    inventory: 4,
    shipping: 6.0
  },
  {
    id: 9,
    title: 'Granite Computer',
    price: 109.1,
    inventory: 10,
    shipping: 22.7
  },
  {
    id: 10,
    title: 'Handcrafted Soft Salad',
    price: 13.99,
    inventory: 3,
    shipping: 3.5
  },
  {
    id: 11,
    title: 'Incredible Steel Bacon',
    price: 30.99,
    inventory: 5,
    shipping: 7.9
  },
  {
    id: 12,
    title: 'Tasty Plastic Bike',
    price: 75.0,
    inventory: 5,
    shipping: 25.0
  }
];

const promotions = [
  { id: 1, title: '30% OFF' },
  { id: 2, title: '$100.00 Discount' },
  { id: 3, title: 'Free Shipping' },
  { id: 4, title: '+ $100.00 on limit' }
];

// Simulate requests

export default {
  getProfile(cb) {
    // axios.get('http://localhost:8081/profile').then(response => {
    //   const profile = {
    //     firstName: response.data[0].firstname,
    //     lastName: response.data[0].lastname,
    //     limit: response.data[0].profilelimit
    //   };
    cb(profile);
  },

  getProducts(cb) {
    // axios.get('http://localhost:8081/products').then(response => {
    //   cb(response.data);
    // });

    cb(products);
  }
};
